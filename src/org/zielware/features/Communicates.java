package org.zielware.features;

public class Communicates{	
	public static final String[] filesArray =
		{ "login.jsp", "register.jsp" };	
	
	///messages starting from 10 line, becouse it
	///make easier to find index of appropriate message	
	public static final String[] messagesArray =
		{"Wypelnij caly formularz! ",
		 "Zle dane logowania!",		 
		 "Taki user istnieje! ", 
		 "Haslo sie zgadza! ",
		 "Haslo sie NIE zgadza! ",		 
		 "Lista user�w: ",
		 "Znalazlem takiego usera ",
		 "Nie udalo sie stworzyc usera!"
		};
}